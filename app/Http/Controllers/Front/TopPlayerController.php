<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePlayerRequest;
use App\Models\Player;
use Illuminate\Http\Request;

class TopPlayerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $players = Player::select('first_name', 'last_name', 'score')->orderBy('score', 'DESC')->limit(3)->get();

        if ($request->ajax()) {
            return response()->json([
                'html_players' => view('front.ajax.table-players', [
                    'players' => $players,
                ])->render(),
            ]);
        }

        return view('front.index', compact('players'));
    }

}
