<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Player;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlayerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort = $request->input('sort', 'id');
        $order = $request->input('order', 'desc');
        $players = Player::orderBy($sort, $order)->paginate(10);

        return view('admin.index', [
            'sort' => $sort,
            'order' => $order,
            'players' => $players,
        ]);
    }

    public function updateScore(Player $player, Request $request)
    {
        if (Auth::user()->is_admin) {
            $validatedData = $request->validate([
                'score' => 'required|integer|min:0'
            ]);

            $player->update([
                'score' => $validatedData['score']
            ]);

            return response()->json([
                'score' => $player->score
            ]);
        } else {
            return response()->json(['message' => 'You are not authorized to update score.'], 403);
        }
    }

    public function export()
    {
        $players = Player::all();

        $headers = [
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="players.csv"',
        ];

        $callback = function() use ($players) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['First Name', 'Last Name', 'Scan Code', 'Email', 'Phone', 'Score', 'Side']);

            foreach ($players as $player) {
                fputcsv($file, [$player->first_name, $player->last_name, $player->scan_code, $player->email, $player->phone, $player->score, $player->side]);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
