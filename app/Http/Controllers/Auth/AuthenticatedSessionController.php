<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthenticatedSessionController extends Controller
{
    public function create()
    {
        return view('auth.login');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'password' => 'required',
        ]);

        if (!auth()->attempt($request->only('name', 'password'), $request->filled('remember'))) {
            throw ValidationException::withMessages([
                'name' => __('auth.failed'),
            ]);
        }

        return redirect()->intended('/admin');
    }

    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }
}
