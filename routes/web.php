<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Admin\PlayerController;
use \App\Http\Controllers\Front\TopPlayerController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TopPlayerController::class, 'show'])
    ->name('top.show');

Route::get('/login', [AuthenticatedSessionController::class, 'create'])
    ->middleware('guest')
    ->name('login');

Route::post('/login', [AuthenticatedSessionController::class, 'store'])
    ->middleware('guest');

Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
    ->middleware('auth')
    ->name('logout');

Route::middleware(['auth', 'is_admin'])->group(function () {
    Route::get('/admin', [PlayerController::class, 'index'])->name('admin.index');
});

Route::get('/admin/players/export', [PlayerController::class, 'export'])->name('admin.players.export');
Route::patch('/admin/players/{player}/score', [PlayerController::class, 'updateScore'])->name('admin.players.update-score');
