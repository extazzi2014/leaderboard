<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Leaderboard') }}</title>
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Montserrat:wght@500;600;700;800&display=swap');
    </style>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
@yield('content')
</body>
@if(Route::currentRouteName() == 'top.show')
    <script>
        function updatePlayersTable() {
            // Use AJAX to fetch the top players
            fetch("{{ route('top.show') }}", { headers: { 'X-Requested-With': 'XMLHttpRequest' } })
                .then((response) => {
                    if (!response.ok) {
                        throw new Error("Network response was not ok");
                    }
                    return response.json();
                })
                .then((data) => {
                    // Update the table with the new data
                    document.getElementById("players-table").innerHTML = data.html_players;
                })
                .catch((error) => {
                    console.error("There was a problem with the fetch operation:", error);
                });
        }

        // Call the updatePlayersTable function every 8 seconds
        setInterval(updatePlayersTable, 8000);
    </script>
@endif
</html>
