@php
    $cnt = 1;
@endphp
@foreach($players as $player)
    <tr>
        <td class="bold">
        {{ $cnt }}</th>
        <td>
            <div class="text">
                <div>{{ $player->first_name }} {{ substr($player->last_name, 0, 1) }}</div>
            </div>
        </td>
        <td class="bold">{{ number_format($player->score, 0, ',', ' ') }}</td>
    </tr>
    @php
        $cnt++;
    @endphp
@endforeach
