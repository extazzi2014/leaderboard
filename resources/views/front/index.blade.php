@extends('front.layout')

@section('content')
    <div class="mainwrap">
        <div class="container">
            <div class="leaderboard-title">leaderboard</div>
            <div class="leaderboard-table">
                <table>
                    <thead>
                    <tr>
                        <th>Place</th>
                        <th>Player</th>
                        <th>Score</th>
                    </tr>
                    </thead>
                    <tbody id="players-table">
                        @include('front.ajax.table-players')
                    </tbody>
                </table>
            </div>
            <!-- end .leaderboard-table-->
            <div class="leaderboard-img"></div>
        </div>
        <!-- end .container-->
    </div>
@endsection
