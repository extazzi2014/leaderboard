@extends('front.layout')

@section('content')
    <div class="mainwrap">
        <form method="POST" action="{{ route('login') }}" class="login">
            {{ csrf_field() }}
            <div class="container">
                <div class="row">
                    <div class="label">Login</div>
                    <input value="{{ old('name') }}" name="name" id="name" type="text">
                </div>
                <!-- end .row-->
                <div class="row">
                    <div class="label">Password</div>
                    <input name="password" id="password" type="password">
                    @error('password')
                    <div class="input-info">{{ $message }}</div>
                    @enderror
                    @error('name')
                    <div class="input-info">{{ $message }}</div>
                    @enderror
                </div>
                <!-- end .row-->
                <div class="submit">
                    <button class="btn" type="submit">Sign In</button>
                </div>
                <!-- end .submit-->
            </div>
            <!-- end .container-->
        </form>
        <!-- end .login-->
    </div>
@endsection
