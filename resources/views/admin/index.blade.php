@extends('front.layout')

@section('content')
    <div class="mainwrap">
        <div class="container">
            @if (Auth::check())
                <div class="logout">
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
                <!-- end .logout-->

                <div class="export"><a href="{{ route('admin.players.export') }}" class="btn"><img
                            src="img/file-csv-white.svg" alt=""/>Export .csv</a></div>
                <!-- end .export-->
            @endif
            <div class="table">
                <table>
                    <thead>
                    <tr>
                        <th><a href="{{ route('admin.index', ['sort' => 'first_name', 'order' => ($sort === 'first_name' && $order === 'asc') ? 'desc' : 'asc']) }}" class="sort-link {{ $sort === 'first_name' ? ($order === 'asc' ? 'up' : 'down') : '' }}">First name</a></th>
                        <th><a href="{{ route('admin.index', ['sort' => 'last_name', 'order' => ($sort === 'last_name' && $order === 'asc') ? 'desc' : 'asc']) }}" class="sort-link {{ $sort === 'last_name' ? ($order === 'asc' ? 'up' : 'down') : '' }}">Last name</a></th>
                        <th><a href="{{ route('admin.index', ['sort' => 'side', 'order' => ($sort === 'side' && $order === 'asc') ? 'desc' : 'asc']) }}" class="sort-link {{ $sort === 'side' ? ($order === 'asc' ? 'up' : 'down') : '' }}">Side</a></th>
                        <th><a href="{{ route('admin.index', ['sort' => 'score', 'order' => ($sort === 'score' && $order === 'asc') ? 'desc' : 'asc']) }}" class="sort-link {{ $sort === 'score' ? ($order === 'asc' ? 'up' : 'down') : '' }}">Score</a></th>
                    </tr>
                    </thead>
                    @if($players->isNotEmpty())
                        @foreach($players as $player)
                            <tr>
                                <td>
                                    <div class="text">
                                        <div>{{ $player->first_name }}</div>
                                    </div>
                                </td>
                                <td>
                                    <div class="text">
                                        <div>{{ $player->last_name }}</div>
                                    </div>
                                </td>
                                <td>
                                    <div class="text">
                                        <div>{{ $player->side }}</div>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" id="score-{{ $player->id }}" value="{{ $player->score }}" inputmode="numeric" pattern="\d*" data-player-id="{{ $player->id }}">
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!-- end .table-->
            <div class="pagination">
                <a href="{{ str_replace('?page=1', '', $players->url(1)) }}" class="first"></a>
                <a href="{{ str_replace('?page=1', '', $players->previousPageUrl()) }}" class="prev"></a>
                <input name="paginate" type="text" value="{{ $players->currentPage() }}" inputmode="numeric"
                       pattern="\d*" data-max="{{ $players->lastPage() }}">
                <button class="btn">GO</button>
                <a href="{{ $players->nextPageUrl() }}" class="next"></a>
                <a href="{{ $players->url($players->lastPage()) }}" class="last"></a>
            </div>
            <!-- end .pagination-->
        </div>
        <!-- end .container-->
    </div>
    <script>
        document.querySelector('.pagination button').addEventListener('click', function () {
            var input = document.querySelector('.pagination input[name="paginate"]');
            var page = parseInt(input.value);
            if (page > 0 && page <= input.getAttribute('data-max')) {
                if (page == 1) {
                    var url = window.location.pathname;
                } else {
                    var url = window.location.pathname + '?page=' + page;
                }
                window.location.href = url;
            } else if (page > input.getAttribute('data-max')) {
                var url = window.location.pathname + '?page=' + input.getAttribute('data-max');
                window.location.href = url;
            }
        });

        const inputs = document.querySelectorAll('input[type="text"]');
        inputs.forEach(input => {
            input.addEventListener('blur', () => {
                const playerId = input.dataset.playerId;
                const score = input.value;
                updatePlayerScore(playerId, score);
            });
        });

        function updatePlayerScore(playerId, score) {
            fetch(`/admin/players/${playerId}/score`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                },
                body: JSON.stringify({
                    score: score
                })
            })
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Network response was not ok');
                    }
                    return response.json();
                })
                .then(data => {
                    const scoreInput = document.querySelector(`#score-${playerId}`);
                    scoreInput.value = data.score;
                })
                .catch(error => {
                    console.error('There was a problem with the fetch operation:', error);
                });
        }
    </script>
@endsection
